package com.login.di.components

import com.login.auth.AccountAuthenticator
import com.login.auth.AccountAuthenticatorService
import com.login.di.modules.AppModule
import com.login.ui.login.LoginFragment
import dagger.Component
import javax.inject.Singleton

@Component(modules = [AppModule::class])
@Singleton
interface ApplicationComponent {

    fun inject(accountManager: AccountAuthenticator)
    fun inject(accountAuthenticatorService: AccountAuthenticatorService)

    fun inject(fragment: LoginFragment)
}