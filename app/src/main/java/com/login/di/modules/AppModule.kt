package com.login.di.modules

import android.content.Context
import com.login.LoginApplication
import com.login.auth.AccountAuthenticator
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class AppModule(private val app: LoginApplication) {

    @Provides
    @Singleton
    fun provideApplication(): LoginApplication = app

    @Provides
    @Singleton
    fun provideApplicationContext(app: LoginApplication): Context = app

    @Provides
    @Singleton
    fun provideAccountAuthenticator(context: Context) = AccountAuthenticator(context)
}