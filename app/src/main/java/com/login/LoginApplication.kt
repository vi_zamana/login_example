package com.login

import android.app.Application
import com.login.di.components.ApplicationComponent
import com.login.di.components.DaggerApplicationComponent
import com.login.di.modules.AppModule

class LoginApplication : Application() {

    companion object {
        lateinit var appComponent: ApplicationComponent
    }

    override fun onCreate() {
        super.onCreate()
        initAppComponent()
    }

    private fun initAppComponent() {
        appComponent = DaggerApplicationComponent.builder()
            .appModule(AppModule(this))
            .build()
    }
}