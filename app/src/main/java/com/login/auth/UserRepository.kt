package com.login.auth

import android.accounts.Account
import android.accounts.AccountManager
import android.content.Context
import javax.inject.Inject
import javax.inject.Singleton

const val ACCOUNT_TYPE = "com.login"

const val AUTH_TOKEN_TYPE = "com.login.access"

@Singleton
class UserRepository @Inject constructor(val context: Context) {

    private var accountManager = AccountManager.get(context)

    fun getAccessToken(): String? {
        return getAccount()?.run {
            accountManager.blockingGetAuthToken(this, AUTH_TOKEN_TYPE, false)
        }
    }

    fun isUserLogged() = accountManager.getAccountsByType(ACCOUNT_TYPE).isNotEmpty()

    fun addAccount(email: String, token: String, id: Int) {
        val account = Account(email, ACCOUNT_TYPE)
        accountManager.getAccountsByType(ACCOUNT_TYPE).forEach()
        {
            it.run {
                if (account.name == this.name) {
                    accountManager.setAuthToken(account, AUTH_TOKEN_TYPE, token)
                    return
                } else {
                    removeAccount(it)
                }
            }
        }
        if (accountManager.addAccountExplicitly(
                account,
                token,
                null
            )
        ) {
            accountManager.setAuthToken(account, AUTH_TOKEN_TYPE, token)
        }
    }

    fun removeAccount() {
        accountManager.getAccountsByType(ACCOUNT_TYPE).forEach { removeAccount(it) }
    }

    private fun getAccount() = accountManager.accounts
        .firstOrNull { it.type == ACCOUNT_TYPE }

    @Suppress("DEPRECATION")
    private fun removeAccount(account: Account) {
        accountManager.removeAccount(account, null, null)
    }
}
