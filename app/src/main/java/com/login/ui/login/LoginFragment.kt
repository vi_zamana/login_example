package com.login.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.login.LoginApplication
import com.login.R
import com.login.databinding.FragmentLoginBinding
import javax.inject.Inject

class LoginFragment : Fragment() {

    companion object {
        const val PARAM_AUTH_TOKEN_TYPE = "param_auth_token_type"
        val PARAM_USERNAME: String? = "param_user_name"
    }

    @Inject
    lateinit var viewModelFactory: LoginViewModelFactory

    private lateinit var viewModel: LoginViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        DataBindingUtil.inflate<FragmentLoginBinding>(
            inflater, R.layout.fragment_login, container, false
        ).apply {
            lifecycleOwner = this@LoginFragment
            viewModel = viewModel
            return root
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        LoginApplication.appComponent.inject(this)
        viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(LoginViewModel::class.java)
    }
}