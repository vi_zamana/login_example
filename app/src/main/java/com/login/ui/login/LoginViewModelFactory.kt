package com.login.ui.login

import com.login.auth.UserRepository
import com.login.ui.base.BaseViewModelFactory
import javax.inject.Inject

class LoginViewModelFactory @Inject constructor(
    private val userRepository: UserRepository
) : BaseViewModelFactory<LoginViewModel>(LoginViewModel::class.java) {

    override fun createViewModel() = LoginViewModel(userRepository)
}